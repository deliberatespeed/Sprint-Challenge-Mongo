const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;

const Budget = new mongoose.Schema({
  title: { type: String, required: true, index: true },
  budgetAmount: { type: Number }
});

module.exports = mongoose.model('Budget', Budget);
